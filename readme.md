# Portable Deep Dive - Upgrades - Minimise the impacts

The application is a simple job manager, supporting

* Add Job
* List Jobs
* Delay Job

The application hold the current state (jobs)

The taskLoop will continue to run until Quit is selected.

It's designed to work on multiple user interfaces, currently supporting Console/Terminal (stdin, stdout)

# Getting started

```
npm install
npm test
npm start
```

## Task 1

Refactor the app to support latest version of date-fns. Tests are currently failing in CI for the PR, why?

Review the PR using the current PR Process:

* https://www.notion.so/portable/WIP-Dependency-PR-Review-Process-b34498ba90fa4062a646523c25681879

Consider 

* How many part of the code have to change for each change in data-fns?
* Can you make it easier the next time this happens?

## Task 2

* An unexpected change to the ecosystem


# Principals for this project*
Some SOLID principals applied are:

* Single Responsibility
* Dependency Inversion
* Interface segregation principle

Some Clean Code principals applied are:

* Minimal parameters: Ideally 0, 1 or 2 sometimes, 3 or more if there's a good reason.
* Short Method: 1-10 lines (Using multiple levels of abstraction)
* No unexpected Side Effects 
