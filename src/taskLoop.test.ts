import {TaskLoop} from "./taskLoop";
import {Task} from "./task";
import {OptionDisplayer} from "./ui/optionDisplayer";
import {OptionSelector} from "./ui/optionSelector";
import {TitleSetter} from "./ui/titleSetter";

describe('Task Loop', () => {

    const promisifyAction = <T>(action: ()=>T) => new Promise<T>((resolve) => {
        resolve(action())
    })

    const log = jest.fn().mockImplementation((msg) => console.log(msg))

    const showMessage: Task = {
        selectPrompt: "Show Message",
        endApplication: false,
        action: () => promisifyAction(() => log("Message"))
    }

    const quit: Task = {
        selectPrompt: "Quit",
        endApplication: true,
        action: () =>  promisifyAction(() => log("Quit"))
    }

    it('should continue until an end task is selected', async () => {
        let callEachOption:number = 0
        const select = jest.fn().mockImplementation(() => {
            return promisifyAction(() => {
                const option = callEachOption
                callEachOption++
                return option
            })
        })
        const ui: OptionDisplayer & OptionSelector & TitleSetter = {
            setTitle: jest.fn(),
            displayOptions: jest.fn(),
            getSelectedOption: select
        }
        const loop = new TaskLoop(ui)
        loop.setTasks([showMessage, quit])
        return loop.run().then(() => {
            expect(ui.displayOptions).toBeCalledTimes(2)
            expect(select).toBeCalledTimes(2)
            expect(log).toBeCalledWith("Message")
            expect(log).toBeCalledWith("Quit")
        })
    });
});