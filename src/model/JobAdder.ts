export interface JobAdder {
    addJob: (name: string, create: Date) => void
}
