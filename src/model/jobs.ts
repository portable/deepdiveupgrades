import {JobAdder} from "./JobAdder";
import {Job} from "./job";
import {JobLister} from "./JobLister";
import {JobUpdater} from "./JobUpdater";

export class Jobs implements JobAdder,JobLister,JobUpdater {

    private readonly jobList: Job[]

    addJob(name: string, due: Date): void {
        this.jobList.push({name, due})
    }

    constructor() {
        this.jobList = [];
    }

    getJobs(): Job[] {
        return this.jobList;
    }

    updateJob(id: number, due: Date): void {
        this.jobList[id].due = due
    }


}