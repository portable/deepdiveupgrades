export type Job = {
    name: string
    due: Date
}