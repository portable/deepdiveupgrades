export interface JobUpdater {
    updateJob: (id: number, create: Date) => void
}