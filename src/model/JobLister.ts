import {Job} from "./job";

export interface JobLister {
    getJobs: () => Job[]
}