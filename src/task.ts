

export interface Task {
    selectPrompt: string
    action: () => Promise<void>
    endApplication: boolean
}