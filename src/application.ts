import {TaskLoop} from "./taskLoop";
import {AddJob} from "./tasks/AddJob";
import {Quit} from "./tasks/Quit";
import {Jobs} from "./model/jobs";
import {Console} from "./ui/console";
import {ListJobs} from "./tasks/ListJobs";
import {DelayJob} from "./tasks/DelayJob";


export class Application {
    private taskLoop: TaskLoop
    private readonly jobs: Jobs
    private ui = new Console()
    private constructor() {
        this.taskLoop = new TaskLoop(this.ui)
        this.jobs = new Jobs()
        this.taskLoop.setTasks([
            new AddJob(this.jobs, this.ui),
            new ListJobs(this.jobs, this.ui),
            new DelayJob(this.jobs, this.ui),
            new Quit()
        ])
    }
    public static Create() {
        return new Application()
    }
    public run() {
        this.taskLoop.run().catch(e => {
            console.warn(e)
        })
    }
    public static CreateAndRun() {
        const app = Application.Create()
        app.run()
    }
}