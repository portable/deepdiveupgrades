import {Task} from "./task";
import {OptionDisplayer} from "./ui/optionDisplayer";
import {OptionSelector} from "./ui/optionSelector";
import {TitleSetter} from "./ui/titleSetter";


export class TaskLoop {
    private tasks: Task[]
    private selected: number|null
    private ui: OptionDisplayer & OptionSelector & TitleSetter

    constructor(ui: OptionDisplayer&OptionSelector & TitleSetter) {
        this.ui = ui;
        this.selected = null
        this.tasks = []
        this.setSelection = this.setSelection.bind(this)
    }

    private setSelection(selected: number) {
     this.selected = selected
    }

    public setTasks(value: Task[]) {
        this.tasks = value;
    }

    public run(): Promise<null> {
        return new Promise(async (resolve) => {
            while (true) {
                this.displayTaskOptions()
                await this.runSelectedAction()
                if (this.isEndTask()) {
                    resolve(null)
                    break
                }
            }
        })
    }

    private displayTaskOptions() {
        this.ui.setTitle("Select Task")
        this.ui.displayOptions(this.tasks.map((task, i) => ({id: i, label: task.selectPrompt})))
    }

    private runSelectedAction() {
        return this.ui.getSelectedOption().then(this.setSelection).then(() => {
            return this.getSelectedTask()?.action()
        })
    }

    private getSelectedTask() {
        if(this.selected === null) {
            return null
        }
        return this.tasks[this.selected]
    }
    private isEndTask() {
        return this.getSelectedTask()?.endApplication || false
    }
}