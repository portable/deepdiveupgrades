import {Task} from "../task";


export class Quit implements Task {
    action(): Promise<void> {
        console.log("Quitting")
        return Promise.resolve(undefined);
    }
    endApplication: boolean = true;
    selectPrompt: string = "Quit";

}