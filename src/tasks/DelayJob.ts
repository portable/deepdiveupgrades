import {Task} from "../task";
import {OptionDisplayer} from "../ui/optionDisplayer";
import {OptionSelector} from "../ui/optionSelector";
import {TitleSetter} from "../ui/titleSetter";
import {JobLister} from "../model/JobLister";
import {DisplayableOption} from "../displayableOption";
import {Job} from "../model/job";
import {JobUpdater} from "../model/JobUpdater";
import {distanceInWordsToNow} from "date-fns";
import {UserInputDateGetter} from "../ui/userInputDateGetter";

export class DelayJob implements Task {
    private jobs: JobUpdater & JobLister
    private ui: UserInputDateGetter & OptionDisplayer & OptionSelector & TitleSetter

    constructor(jobAdded: JobUpdater & JobLister, ui: UserInputDateGetter & OptionDisplayer & OptionSelector & TitleSetter) {
        this.jobs = jobAdded;
        this.ui = ui
    }

    async action(): Promise<void> {
        this.ui.setTitle("Select Job")
        this.ui.displayOptions(this.jobs.getJobs().map(DelayJob.jobsToOptions))
        const option = await this.ui.getSelectedOption()

        const dueDate = await this.ui.getUserInputDate("Due Date")
        return this.jobs.updateJob(option, dueDate)
    }
    endApplication: boolean = false;
    selectPrompt: string = "Delay a job";

    private static jobsToOptions(job: Job, index: number): DisplayableOption {
        return {
            id: index,
            label: `${job.name} - ${distanceInWordsToNow(job.due)}`
        }
    }
}