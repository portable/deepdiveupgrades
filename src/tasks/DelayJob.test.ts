import {DelayJob} from "./DelayJob";
import {JobUpdater} from "../model/JobUpdater";
import {JobLister} from "../model/JobLister";
import {testJobs} from "../test-data/testJobs";
import {UserInputDateGetter} from "../ui/userInputDateGetter";
import {OptionDisplayer} from "../ui/optionDisplayer";
import {OptionSelector} from "../ui/optionSelector";
import {TitleSetter} from "../ui/titleSetter";

describe('Delay Job', () => {
    it('should update the due date on the selected job', async () => {
        const jobs: JobUpdater & JobLister = {
            getJobs: jest.fn().mockImplementation(() => testJobs),
            updateJob: jest.fn()
        }
        const ui: UserInputDateGetter & OptionDisplayer & OptionSelector & TitleSetter = {
            getUserInputDate: jest.fn().mockImplementation(() => Promise.resolve(new Date(2021, 8, 23))),
            getSelectedOption: jest.fn().mockImplementation(() => Promise.resolve(0)),
            displayOptions: jest.fn(),
            setTitle: jest.fn()
        }
        const delayJob = new DelayJob(jobs, ui)
        await delayJob.action()
        expect(jobs.updateJob).toBeCalledWith(0, new Date(2021, 8, 23))
        expect(jobs.getJobs).toBeCalledTimes(1)
        expect(ui.getUserInputDate).toBeCalledWith("Due Date")
        expect(ui.setTitle).toBeCalledWith('Select Job')
        expect(ui.displayOptions).toBeCalledTimes(1)
        expect(ui.getSelectedOption).toBeCalledTimes(1)
    });
});