import {Task} from "../task";
import {JobLister} from "../model/JobLister";
import {OptionDisplayer} from "../ui/optionDisplayer";
import {TitleSetter} from "../ui/titleSetter";
import {Job} from "../model/job";
import {DisplayableOption} from "../displayableOption";
import {format} from "date-fns";

export class ListJobs implements Task {

    jobLister: JobLister
    ui: OptionDisplayer&TitleSetter

    constructor(jobLister: JobLister, ui: OptionDisplayer&TitleSetter) {
        this.jobLister = jobLister;
        this.ui = ui;
    }

    action(): Promise<void> {
        const jobs = this.jobLister.getJobs()
        this.ui.setTitle("Jobs")
        this.ui.displayOptions(jobs.map(ListJobs.jobToDisplayOption))
        return Promise.resolve()
    }
    endApplication: boolean = false;
    selectPrompt: string = "List jobs";

    private static jobToDisplayOption(job: Job, index: number): DisplayableOption {
        return {id: index, label: ListJobs.jobToLabel(job)}
    }

    private static jobToLabel(job: Job): string {
        return `${job.name} - ${format(job.due, 'dddd MMMM D')}`;
    }
}