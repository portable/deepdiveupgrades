import {Task} from "../task";
import {JobAdder} from "../model/JobAdder";
import {UserInputGetter} from "../ui/userInputGetter";
import {UserInputDateGetter} from "../ui/userInputDateGetter";

export class AddJob implements Task {
    private jobAdded: JobAdder
    private ui: UserInputGetter & UserInputDateGetter

    constructor(jobAdded: JobAdder, ui: UserInputGetter & UserInputDateGetter) {
        this.jobAdded = jobAdded;
        this.ui = ui
    }

    async action(): Promise<void> {
        const name = await this.ui.getUserInput("Job Name")
        const dueDate = await this.ui.getUserInputDate("Due Date")
        return this.jobAdded.addJob(name, dueDate)
    }
    endApplication: boolean = false;
    selectPrompt: string = "Create a new job";

}