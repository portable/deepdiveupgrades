import {JobLister} from "../model/JobLister";
import {OptionDisplayer} from "../ui/optionDisplayer";
import {ListJobs} from "./ListJobs";
import {TitleSetter} from "../ui/titleSetter";
import {DisplayableOption} from "../displayableOption";
import {testJobs} from "../test-data/testJobs";

describe('List Jobs', () => {


    it('should list jobs', async () => {
        const jobList: JobLister = {
            getJobs: jest.fn().mockImplementation(() => testJobs)
        }
        let display = ""
        const ui: OptionDisplayer & TitleSetter = {
            displayOptions: jest.fn().mockImplementation((options: DisplayableOption[]) => display += options.map(option => option.label)+"\n"),
            setTitle: jest.fn()
        }
        const listJobs = new ListJobs(jobList, ui)
        await listJobs.action()
        expect(jobList.getJobs).toBeCalled()
        expect(display).toContain("Test Job - Monday August 23")
    });
});