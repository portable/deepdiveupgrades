import {AddJob} from "./AddJob";
import {JobAdder} from "../model/JobAdder";
import {UserInputGetter} from "../ui/userInputGetter";
import {UserInputDateGetter} from "../ui/userInputDateGetter";

describe('Add Job', () => {
    it('should add a job', async () => {
        // @ts-ignore
        const adder: JobAdder = {
            addJob: jest.fn()
        }
        const ui:UserInputGetter & UserInputDateGetter = {
            getUserInput: jest.fn().mockImplementation(() => Promise.resolve("New Test Name")),
            getUserInputDate: jest.fn().mockImplementation(() => Promise.resolve(new Date(2013,7,23)))
        }
        const newJob = new AddJob(adder, ui)
        await newJob.action()
        expect(ui.getUserInput).toBeCalledWith("Job Name")
        expect(adder.addJob).toBeCalledWith("New Test Name",  new Date(2013,7,23))
    });
});