const readline = require('readline');

export function askQuestion(question: string): Promise<string> {
    return new Promise<string>((resolve) => {

        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question(question, (answer: string) => {
            resolve(answer)
            rl.close();
        });
    })
}

