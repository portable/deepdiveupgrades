export interface UserInputDateGetter {
    getUserInputDate: (label: string) => Promise<Date>
}