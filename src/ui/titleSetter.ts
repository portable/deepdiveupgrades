export interface TitleSetter {
    setTitle: (notice: string) => void
}