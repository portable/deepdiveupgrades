import {OptionDisplayer} from "./optionDisplayer";
import {OptionSelector} from "./optionSelector";
import {DisplayableOption} from "../displayableOption";
import {askQuestion} from "./askQuestion";
import {TitleSetter} from "./titleSetter";
import {UserInputGetter} from "./userInputGetter";
import parse from "date-fns/parse";
import {UserInputDateGetter} from "./userInputDateGetter";

export class Console implements OptionDisplayer, OptionSelector, TitleSetter, UserInputGetter, UserInputDateGetter {
    displayOptions(options: DisplayableOption[]): void {
        console.log(options.map(({id, label})=> `  ${id+1}: ${label}`).join("\n"))
    }
    getSelectedOption(): Promise<number> {
        return askQuestion("Enter selection: ").then((answer) => {
            const option = Number.parseInt(answer)
            if(isNaN(option)) {
                console.log("Option is invalid\n")
                return this.getSelectedOption()
            }
            return Console.optionToId(option)
        })
    }
    private static optionToId(option: number): number {
        return option - 1
    }
    setTitle(title: string): void {
        console.log(title)
        console.log("=".repeat(title.length))
    }

    getUserInput(label: string): Promise<string> {
        return askQuestion(label+': ')
    }

    getUserInputDate(label: string): Promise<Date> {
        return askQuestion(label+' (yyyy-mm-dd): ').then(answer => {
            try {
                return parse(answer)
            } catch (e: unknown) {
                console.log("Date is invalid\n")
                return this.getUserInputDate(label)
            }
        })
    }
}