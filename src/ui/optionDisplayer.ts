import {DisplayableOption} from "../displayableOption";

export interface OptionDisplayer {
    displayOptions: (options: DisplayableOption[]) => void
}

