export interface OptionSelector {
    getSelectedOption: () => Promise<number>
}