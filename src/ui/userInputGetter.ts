export interface UserInputGetter {
    getUserInput: (label: string) => Promise<string>
}
